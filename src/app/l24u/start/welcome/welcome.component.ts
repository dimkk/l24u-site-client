import { Component, ViewEncapsulation } from '@angular/core';

import { BaAppPicturePipe } from '../../../theme/pipes';

@Component({
  selector: 'welcome',
  encapsulation: ViewEncapsulation.None,
  pipes: [BaAppPicturePipe],
  styles: [require('./welcome.scss')],
  template: require('./welcome.html')
})
export class welcome {

  ngOnInit() {
  }
}
