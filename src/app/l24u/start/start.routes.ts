import { RouterConfig } from '@angular/router';
import { Start } from './start.component';

// noinspection TypeScriptValidateTypes
export const StartRoutes: RouterConfig = [
  {
    path: 'start',
    component: Start
  }
];
