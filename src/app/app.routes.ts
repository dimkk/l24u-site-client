import { provideRouter, RouterConfig } from '@angular/router';
import { LoginRoutes } from './pages/login/login.routes';
import { PagesRoutes } from './pages/pages.routes';
import { RegisterRoutes } from './pages/register/register.routes';
import { StartRoutes } from './l24u/start/start.routes';

export const routes: RouterConfig = [
  ...LoginRoutes,
  ...RegisterRoutes,
  ...PagesRoutes,
  ...StartRoutes,
  {
    path: '**',
    redirectTo: '/start'
  }
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
